from PyQt4 import uic
from PyQt4.QtCore import *
import analyseText

( Ui_MainWindow, QMainWindow ) = uic.loadUiType( 'mainwindow.ui' )

class MainWindow ( QMainWindow ):
    """MainWindow inherits QMainWindow"""

    def __init__ ( self, parent = None ):
        QMainWindow.__init__( self, parent )
        self.ui = Ui_MainWindow()
        self.ui.setupUi( self )
        self.connect(self.ui.pushButton ,SIGNAL('clicked()'),self.runtest)


    def runtest(self):
        
        sef =self.ui.plainTextEdit.toPlainText()
        print(sef)
        output1 = analyseText.syllabs(sef)
        output2 =analyseText.words(sef)
        output3 = analyseText.clauses(sef)
        output4 = analyseText.sentences(sef)
        
        if(output1 !=0 and output2 != 0 and output3 !=0 and output4 !=0):
            average1 = analyseText.averageSylWor(sef)
            average2 = analyseText.averageWorCla(sef)
            average3 = analyseText.averageClaPhr(sef)
        else:
            average1 = -1
            average2 = -1
            average3 = -1
       
        self.ui.lcdNumber.display(output1)
        self.ui.lcdNumber_2.display(output2)
        self.ui.lcdNumber_3.display(output3)
        self.ui.lcdNumber_4.display(output4)
        self.ui.lcdNumber_5.display(average1)
        self.ui.lcdNumber_6.display(average2)
        self.ui.lcdNumber_7.display(average3)
        print("dadadadad",average2)
    def __del__ ( self ):
        self.ui = None
