from __future__ import division
def syllabs(s):
    sylabNo=0
    for i in range (0, len(s)):
        if(s[i]=="|"):
            sylabNo+=1
    return sylabNo

def words(s):
    wordNo=1
    for i in range (0, len(s)):
        if(s[i]==" "):
            wordNo+=1
    return wordNo

def clauses(s):
    clauseNo=0
    for i in range (0, len(s)):
        if(s[i]=="/"):
            clauseNo+=1;
    return clauseNo

def sentences(s):
    sentenceNo=0
    for i in range (0, len(s)):
        if (s[i]=="."):
            sentenceNo+=1;
    return sentenceNo

def averageSylWor(s):
    return (syllabs(s)/words(s))
    
def averageWorCla(s):
    return (words(s)/clauses(s))
def averageClaPhr(s):
    return (clauses(s)/sentences(s))
    